-- MySQL dump 10.13  Distrib 8.0.12, for macos10.13 (x86_64)
--
-- Host: localhost    Database: redenvelope
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `re_airdrop`
--

DROP TABLE IF EXISTS `re_airdrop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `re_airdrop` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '空投信息',
  `user_id` int(11) DEFAULT NULL COMMENT '发起者',
  `title` varchar(45) DEFAULT NULL COMMENT '糖果名称',
  `type` varchar(45) DEFAULT NULL COMMENT '糖果类型',
  `num` double DEFAULT NULL COMMENT '糖果数量',
  `left_num` double DEFAULT NULL COMMENT '红包剩余数量',
  `start_time` bigint(13) DEFAULT NULL COMMENT '开奖时间',
  `banner` text COMMENT 'banner图',
  `introduction` text COMMENT '介绍',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态。0 未开奖 1 开奖中 -1 过期',
  `create_time` bigint(13) DEFAULT NULL,
  `update_time` bigint(13) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `re_airdrop`
--

LOCK TABLES `re_airdrop` WRITE;
/*!40000 ALTER TABLE `re_airdrop` DISABLE KEYS */;
INSERT INTO `re_airdrop` VALUES (1,2,'fdfdfd','fdfd',11,7.7,1539566728343,'rererer','rerere',0,1539568259518,1539568441719),(2,1,'fdfdfdfdfdf','fdfd',11,11,1539566728343,'rererer','rerere',0,1539653015847,1539653810376);
/*!40000 ALTER TABLE `re_airdrop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `re_order`
--

DROP TABLE IF EXISTS `re_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `re_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '抢空投订单',
  `airdrop_id` int(11) DEFAULT NULL COMMENT '空投id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `address` text COMMENT '钱包地址',
  `num` double DEFAULT NULL COMMENT '红包数量',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态 0 等待开奖 1 已开奖',
  `create_time` bigint(13) DEFAULT NULL,
  `update_time` bigint(13) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `re_order`
--

LOCK TABLES `re_order` WRITE;
/*!40000 ALTER TABLE `re_order` DISABLE KEYS */;
INSERT INTO `re_order` VALUES (1,1,2,'fdfd',11,1,1539568927492,1539568986239),(2,1,2,'fdfd',0,0,1539744101238,1539744101238),(3,1,2,'fdfd',0,0,1539744126491,1539744126491),(4,1,2,'fdfd',0,0,1539744761988,1539744761988),(5,1,2,'fdfd',0,0,1539744807543,1539744807543),(6,1,2,'fdfd',0,0,1539744810497,1539744810497),(7,1,2,'fdfd',0,0,1539744827657,1539744827657),(8,1,2,'fdfd',0,0,1539744829066,1539744829066),(9,1,2,'fdfd',0,0,1539744861719,1539744861719),(10,1,2,'fdfd',0,0,1539744862835,1539744862835),(11,1,2,'fdfd',0,0,1539744863767,1539744863767),(12,1,2,'fdfd',0,0,1539745155789,1539745155789),(13,1,2,'fdfd',0,0,1539745235000,1539745235000),(14,1,2,'fdfd',0,0,1539745250286,1539745250286),(15,1,2,'fdfd',0,0,1539745253886,1539745253886),(16,1,2,'fdfd',0,0,1539745254775,1539745254775),(17,1,2,'fdfd',0,0,1539745255525,1539745255525),(18,1,2,'fdfd',3.3,0,1539745327184,1539745327184);
/*!40000 ALTER TABLE `re_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `re_user`
--

DROP TABLE IF EXISTS `re_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `re_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户',
  `mobile` varchar(45) DEFAULT NULL COMMENT '手机号',
  `name` varchar(45) DEFAULT NULL COMMENT '用户名',
  `password` varchar(45) DEFAULT NULL,
  `avatar` text COMMENT '头像',
  `introduction` varchar(225) DEFAULT NULL COMMENT '简介',
  `type` tinyint(1) DEFAULT NULL COMMENT '0 前端用户 1 后台用户',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态 0 不可用 1 可用',
  `create_time` bigint(13) DEFAULT NULL,
  `update_time` bigint(13) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `re_user`
--

LOCK TABLES `re_user` WRITE;
/*!40000 ALTER TABLE `re_user` DISABLE KEYS */;
INSERT INTO `re_user` VALUES (1,'122','test','1234','11','111',1,0,1539566728343,1539567659273),(2,'18661772011',NULL,NULL,NULL,NULL,0,1,1539584641871,1539584641871);
/*!40000 ALTER TABLE `re_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-22  9:47:13
